package example;

public class Counter {

    // Counter limited to 0..100
    // Inits to 0

    Value val = new Value(0, 100);

    public void add(int n) {
        val.set(val.get() + n);
    }

    public void dec(int n) {
        val.set(val.get() - n);
    }

    public int getValue() {
        return val.get();
    }
}