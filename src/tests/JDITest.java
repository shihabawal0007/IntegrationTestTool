package tests;

import com.sun.jdi.ClassType;
import com.sun.jdi.Field;
import com.sun.jdi.ObjectReference;
import com.sun.jdi.ReferenceType;
import com.sun.jdi.Value;
import com.sun.jdi.VirtualMachine;
import com.sun.jdi.Bootstrap;
import com.sun.jdi.connect.*;
import com.sun.jdi.request.*;

import org.testng.internal.Arguments;

import com.sun.jdi.event.ClassPrepareEvent;
import com.sun.jdi.event.Event;
import com.sun.jdi.event.EventQueue;
import com.sun.jdi.event.EventSet;
import com.sun.jdi.event.LocatableEvent;
import com.sun.jdi.event.MethodEntryEvent;
import com.sun.jdi.event.MethodExitEvent;
import com.sun.jdi.event.VMDeathEvent;
import com.sun.jdi.event.VMDisconnectEvent;
import com.sun.jdi.event.VMStartEvent;

import java.util.Map;
import java.util.List;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;

public class JDITest {

    static boolean watch = true;

    private static VirtualMachine vm;

    // args[0] = class to test

    public static void main(String args[]) {
        run(args[0]);
        // run("org.testng.TestNG -testclass tests.CounterUT -log 2");
    }

    public static void run(String prog) {
        // Class patterns for which we don't want events
        // String[] excludes = {"java.*", "javax.*", "sun.*", "com.sun.*"};

        vm = launchTarget("org.testng.TestNG -testclass " + prog + " -log 2");
        System.out.println("vm name=" + vm.name() + ", test class=" + prog);

        Process process = vm.process();
        StreamRedirectThread errThread = new StreamRedirectThread("error reader", process.getErrorStream(), System.err);
        StreamRedirectThread outThread = new StreamRedirectThread("output reader", process.getInputStream(),
                System.out);
        errThread.start();
        outThread.start();

        // Uncomment the following to watch the endless details...
        // vm.setDebugTraceMode(TRACE_ALL);

        EventRequestManager erm = vm.eventRequestManager();

        MethodEntryRequest req_entry = erm.createMethodEntryRequest();
        req_entry.addClassFilter(prog); // Or use addClassExclusionFilter(excludes) to enable all application
                                        // classes
        req_entry.enable();

        MethodExitRequest req_exit = erm.createMethodExitRequest();
        req_exit.addClassFilter(prog);
        req_exit.enable();

        ClassPrepareRequest req_class = erm.createClassPrepareRequest();
        req_class.addClassFilter(prog);
        req_class.enable();

        EventQueue eq = vm.eventQueue();

        boolean running = true; // End execution on VM end event

        try {

            while (running) {

                EventSet eventSet = eq.remove();

                for (Event event : eventSet) {
                    if (event instanceof VMStartEvent) {
                        System.out.println("VM started");
                    } else if (event instanceof VMDeathEvent || event instanceof VMDisconnectEvent) {
                        System.out.println("VM ended");
                        running = false;
                    } else if (event instanceof ClassPrepareEvent) {
                        ClassPrepareEvent cpe = (ClassPrepareEvent) event;
                        ReferenceType refType = cpe.referenceType();
                        System.out.println("Class " + refType.name() + " loaded.");
                    } else if (event instanceof MethodEntryEvent) {
                        MethodEntryEvent mee = (MethodEntryEvent) event;
                        System.out.println("Entry event: " + mee.method());
                    } else if (event instanceof MethodExitEvent) {
                        MethodExitEvent mee = (MethodExitEvent) event;
                        if (watch)
                            System.out.println("Exit event: " + mee.method());
                        if (watch)
                            System.out.println("   returns " + mee.returnValue());
                    } else {
                        System.out.println("Unhandled event: " + event);
                    }
                }

                vm.resume();

            }
            errThread.join();
            outThread.join();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    static VirtualMachine launchTarget(String mainArgs) {
        LaunchingConnector connector = findLaunchingConnector();
        Map<String, Connector.Argument> arguments = connectorArguments(connector, mainArgs);
        // Print all arguments passed to connector
        for (Map.Entry<String, Connector.Argument> arg : arguments.entrySet()) {
            System.out.println("Argument: " + arg.getValue());
        }
        try {
            return connector.launch(arguments);
        } catch (IOException exc) {
            throw new Error("Unable to launch target VM: " + exc);
        } catch (IllegalConnectorArgumentsException exc) {
            throw new Error("Internal error: " + exc);
        } catch (VMStartException exc) {
            throw new Error("Target VM failed to initialize: " +
                    exc.getMessage());
        }
    }

    static LaunchingConnector findLaunchingConnector() {
        List<Connector> connectors = Bootstrap.virtualMachineManager().allConnectors();
        for (Connector connector : connectors) {
            if (connector.name().equals("com.sun.jdi.CommandLineLaunch")) {
                return (LaunchingConnector) connector;
            }
        }
        throw new Error("No launching connector");
    }

    static Map<String, Connector.Argument> connectorArguments(LaunchingConnector connector, String mainArgs) {
        Map<String, Connector.Argument> arguments = connector.defaultArguments();
        Connector.Argument mainArg = (Connector.Argument) arguments.get("main");
        if (mainArg == null) {
            throw new Error("Bad launching connector");
        }
        mainArg.setValue(mainArgs);
        // System.out.println("Main arg: " + mainArg);
        Connector.Argument optionArg = (Connector.Argument) arguments.get("options");
        if (optionArg == null) {
            throw new Error("Bad launching connector");
        }
        // set cp so the vm can find the main class - assuming that it is on the cp
        // ProgJdb was started with!
        String cp = System.getProperty("java.class.path");
        optionArg.setValue("-cp \"" + cp + "\"");
        // System.out.println("Optional arg: " + optionArg);
        return arguments;
    }

    static Value getValue(LocatableEvent e, String name) {
        Value v = null;
        try {
            ReferenceType refType = e.location().declaringType();
            Field f1 = refType.fieldByName(name);
            ObjectReference o = e.thread().frame(0).thisObject();
            v = o.getValue(f1);
        } catch (Exception e1) {
            e1.printStackTrace();
            v = null;
        }
        return v;
    }

    static ClassType getClass(String name) {
        List<?> classes = vm.classesByName(name);
        if (classes.size() == 0)
            return null;
        return (ClassType) classes.get(0);
    }

}