package tests;

import com.sun.jdi.AbsentInformationException;
import com.sun.jdi.ClassType;
import com.sun.jdi.Field;
import com.sun.jdi.LocalVariable;
import com.sun.jdi.Location;
import com.sun.jdi.ObjectReference;
import com.sun.jdi.ReferenceType;
import com.sun.jdi.StackFrame;
import com.sun.jdi.Value;
import com.sun.jdi.VirtualMachine;
import com.sun.jdi.Bootstrap;
import com.sun.jdi.connect.*;
import com.sun.jdi.request.*;

import com.sun.jdi.event.AccessWatchpointEvent;
import com.sun.jdi.event.BreakpointEvent;
import com.sun.jdi.event.ClassPrepareEvent;
import com.sun.jdi.event.Event;
import com.sun.jdi.event.EventQueue;
import com.sun.jdi.event.EventSet;
import com.sun.jdi.event.LocatableEvent;
import com.sun.jdi.event.MethodEntryEvent;
import com.sun.jdi.event.MethodExitEvent;
import com.sun.jdi.event.ModificationWatchpointEvent;
import com.sun.jdi.event.StepEvent;
import com.sun.jdi.event.VMDeathEvent;
import com.sun.jdi.event.VMDisconnectEvent;
import com.sun.jdi.event.VMStartEvent;
import com.sun.jdi.event.WatchpointEvent;

import java.util.Map;
import java.util.Vector;
import java.util.List;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;

public class IntegrationCoverageMeasurer {

   static boolean watch = true;

   static private Vector<String[]> baseline = new Vector<String[]>();
   static private boolean[] covered;

   private static VirtualMachine vm;

   // args[0] = lower unit test program to run (provides coverage baseline)
   // args[1] = upper unit test program to run (measure the achieved coverage
   // compared to baseline)
   // args[2] = class to trace entry/exit points (name of lower class)

   // java -cp 'bin;.\lib\testng.jar;.\lib\guice.jar;.\lib\jcommander.jar'
   // tests.IntegrationCoverageMeasurer tests.ValueUT tests.CounterUT
   // tests.CounterUT

   public static void main(String args[]) {
      run(args[0], args[2], true);
      covered = new boolean[baseline.size()];
      run(args[1], args[2], false);
      int ccount = 0;
      for (int i = 0; i < covered.length; i++) {
         for (String s : baseline.get(i))
            System.out.print(s + " ");
         if (covered[i]) {
            ccount++;
            System.out.println(" COVERED");
         } else {
            System.out.println(" NOT COVERED");
         }
      }
      double cov = (double) ccount / (double) covered.length;
      System.out.println("Integration coverage of " + args[2] + " by " + args[1] + " = " + 100.0 * cov + "%");
   }

   public static void run(String prog, String calledClass, boolean settingBaseline) {
      // Class patterns for which we don't want events
      // String[] excludes = {"java.*", "javax.*", "sun.*", "com.sun.*"};

      vm = launchTarget("org.testng.TestNG -testclass " + prog + " -log 2");
      System.out.println("vm name=" + vm.name() + ", test class=" + prog + ", monitored class=" + calledClass);

      Process process = vm.process();
      StreamRedirectThread errThread = new StreamRedirectThread("error reader", process.getErrorStream(), System.err);
      StreamRedirectThread outThread = new StreamRedirectThread("output reader", process.getInputStream(), System.out);
      errThread.start();
      outThread.start();

      // Uncomment the following to watch the endless details...
      // vm.setDebugTraceMode(TRACE_ALL);

      EventRequestManager erm = vm.eventRequestManager();

      MethodEntryRequest req_entry = erm.createMethodEntryRequest();
      req_entry.addClassFilter(calledClass); // Or use addClassExclusionFilter(excludes) to enable all application
                                             // classes
      req_entry.enable();

      MethodExitRequest req_exit = erm.createMethodExitRequest();
      req_exit.addClassFilter(calledClass);
      req_exit.enable();

      ClassPrepareRequest req_class = erm.createClassPrepareRequest();
      req_class.addClassFilter(calledClass);
      req_class.enable();

      EventQueue eq = vm.eventQueue();

      boolean running = true; // End execution on VM end event
      StepRequest req_step = null;

      try {

         while (running) {

            EventSet eventSet = eq.remove();

            for (Event event : eventSet) {
               if (event instanceof VMStartEvent) {
                  System.out.println("VM started");
               } else if (event instanceof VMDeathEvent || event instanceof VMDisconnectEvent) {
                  System.out.println("VM ended");
                  running = false;
               } else if (event instanceof ClassPrepareEvent) {
                  ClassPrepareEvent cpe = (ClassPrepareEvent) event;
                  ReferenceType refType = cpe.referenceType();
                  System.out.println("Class " + refType.name() + " loaded.");
               } else if (event instanceof MethodEntryEvent) {
                  MethodEntryEvent mee = (MethodEntryEvent) event;
                  System.out.println("Entry event: " + mee.method());
                  try {
                     List<LocalVariable> parameters = mee.method().arguments();
                     for (LocalVariable p : parameters) {
                        System.out.print("   [" + p.typeName() + "]" + p.name() + "=");
                        System.out.println(getLocalValue((MethodEntryEvent) event, p.name()));
                     }
                     int count = parameters.size();
                     // Record contains the coverage signature
                     String[] record = new String[count + 1];
                     record[0] = mee.method().toString();
                     for (int i = 1; i <= count; i++)
                        record[i] = getLocalValue((MethodEntryEvent) event, parameters.get(i - 1).name()).toString();
                     if (settingBaseline) {
                        // Should really do this in Exit event and add the return value as part of the
                        // signature...
                        if (find(record) == -1) {
                           baseline.add(record); // store the signature
                           System.out.print("Signature: ");
                           for (String s : record)
                              if (watch)
                                 System.out.print(s + " ");
                           if (watch)
                              System.out.println();
                        } else {
                           if (watch)
                              System.out.println("Duplicate coverage signature, not added");
                        }
                     } else {
                        // Check coverage against baseline
                        if (watch)
                           System.out.print("Checking: ");
                        for (String s : record)
                           if (watch)
                              System.out.print(s + " ");
                        int x = find(record);
                        if (x >= 0) {
                           covered[x] = true;
                           if (watch)
                              System.out.print(" -- found --");
                        } else if (watch)
                           System.out.print(" -- not found --");
                        if (watch)
                           System.out.println();
                     }
                  } catch (AbsentInformationException e) {
                     // No parameters
                     System.out.println("error=" + e.getMessage());
                  }
               } else if (event instanceof MethodExitEvent) {
                  MethodExitEvent mee = (MethodExitEvent) event;
                  if (watch)
                     System.out.println("Exit event: " + mee.method());
                  if (watch)
                     System.out.println("   returns " + mee.returnValue());
               } else {
                  System.out.println("Unhandled event: " + event);
               }
            }

            vm.resume();

         }
         errThread.join();
         outThread.join();
      } catch (Exception e) {
         e.printStackTrace();
      }

   }

   // Compare string arrays
   static boolean csa(String a[], String b[]) {
      if (a.length != b.length)
         return false;
      for (int i = 0; i < a.length; i++)
         if (!a[i].equals(b[i]))
            return false;
      return true;
   }

   // Find a signature in the baseline
   static int find(String[] sig) {
      for (int i = 0; i < baseline.size(); i++)
         if (csa(baseline.get(i), sig))
            return i;
      return -1;
   }

   static VirtualMachine launchTarget(String mainArgs) {
      LaunchingConnector connector = findLaunchingConnector();
      Map<String, Connector.Argument> arguments = connectorArguments(connector, mainArgs);
      try {
         return connector.launch(arguments);
      } catch (IOException exc) {
         throw new Error("Unable to launch target VM: " + exc);
      } catch (IllegalConnectorArgumentsException exc) {
         throw new Error("Internal error: " + exc);
      } catch (VMStartException exc) {
         throw new Error("Target VM failed to initialize: " +
               exc.getMessage());
      }
   }

   static LaunchingConnector findLaunchingConnector() {
      List<Connector> connectors = Bootstrap.virtualMachineManager().allConnectors();
      for (Connector connector : connectors) {
         if (connector.name().equals("com.sun.jdi.CommandLineLaunch")) {
            return (LaunchingConnector) connector;
         }
      }
      throw new Error("No launching connector");
   }

   static Map<String, Connector.Argument> connectorArguments(LaunchingConnector connector, String mainArgs) {
      Map<String, Connector.Argument> arguments = connector.defaultArguments();
      Connector.Argument mainArg = (Connector.Argument) arguments.get("main");
      if (mainArg == null) {
         throw new Error("Bad launching connector");
      }
      mainArg.setValue(mainArgs);
      Connector.Argument optionArg = (Connector.Argument) arguments.get("options");
      if (optionArg == null) {
         throw new Error("Bad launching connector");
      }
      // set cp so the vm can find the main class - assuming that it is on the cp
      // ProgJdb was started with!
      String cp = System.getProperty("java.class.path");
      optionArg.setValue("-cp \"" + cp + "\"");

      return arguments;
   }

   static Value getLocalValue(LocatableEvent e, String name) {
      StackFrame stackFrame;
      Value v = null;
      try {
         stackFrame = e.thread().frame(0);
         LocalVariable localVar = stackFrame.visibleVariableByName(name);
         v = stackFrame.getValue(localVar);
      } catch (Exception e1) {
         v = null;
      }
      return v;
   }

   static Value getValue(LocatableEvent e, String name) {
      Value v = null;
      try {
         ReferenceType refType = e.location().declaringType();
         Field f1 = refType.fieldByName(name);
         ObjectReference o = e.thread().frame(0).thisObject();
         v = o.getValue(f1);
      } catch (Exception e1) {
         e1.printStackTrace();
         v = null;
      }
      return v;
   }

   static Value getStaticValue(LocatableEvent e, String name) {
      Value v = null;
      try {
         ReferenceType refType = e.location().declaringType();
         Field f1 = getClass("sb.Demo").fieldByName(name);
         v = refType.getValue(f1);
      } catch (Exception e1) {
         e1.printStackTrace();
         v = null;
      }
      return v;
   }

   static ClassType getClass(String name) {
      List<?> classes = vm.classesByName(name);
      if (classes.size() == 0)
         return null;
      return (ClassType) classes.get(0);
   }

   static Location getLocation(String lclass, int lineNumber) {
      ClassType theClass = getClass(lclass);
      List<?> listOfLocations;
      try {
         listOfLocations = theClass.locationsOfLine(lineNumber);
         if (listOfLocations.size() == 0) {
            System.out.println("No element in the list of locations ");
            return null;
         }
         return (Location) listOfLocations.get(0);
      } catch (AbsentInformationException e) {
         e.printStackTrace();
      }
      return null;
   }

}

class StreamRedirectThread extends Thread {

   private final Reader in;
   private final Writer out;

   private static final int BUFFER_SIZE = 2048;

   StreamRedirectThread(String name, InputStream in, OutputStream out) {
      super(name);
      this.in = new InputStreamReader(in);
      this.out = new OutputStreamWriter(out);
      setPriority(Thread.MAX_PRIORITY - 1);
   }

   @Override
   public void run() {
      try {
         char[] cbuf = new char[BUFFER_SIZE];
         int count;
         while ((count = in.read(cbuf, 0, BUFFER_SIZE)) >= 0) {
            out.write(cbuf, 0, count);
         }
         out.flush();
      } catch (IOException exc) {
         System.err.println("Child I/O Transfer - " + exc);
      }
   }
}
