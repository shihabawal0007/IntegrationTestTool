package example;
public class Demo {

    private int iList[] = { 3, 5, 7, 11, 13, 17 }; // list of interesting values
    private static int zcount = 0;
    private int val = 0;

    public void inc() {
        val++;
    }

    public void dec() {
        val--;
    }

    public int getValue() {
        return val;
    }

    public void zero() {
        val = 0;
        zcount++;
    }

    // Return true iff val is in iList
    public boolean isInteresting() {
        boolean temp = false;
        for (int l : iList)
            if (val == l)
                temp = true;
        return temp;
    }

    public double checkArguments(String name, int x, double d) {
        int i = 0;
        return d;
    }

    public long bar(long a, long b, long c) {
        return a + b / c;

    }

    public long foo(long x, long y) {
        return bar(1L, x, y);
    }

    void testFoo() {
        foo(5L, 3L);
        foo(3L, 5L); // 2.1 confuse inputs
    }

    public static void main(String args[]) {
        Demo d = new Demo();
        String hello = "Hello";
        d.zero();
        double temp = d.checkArguments("Alice", 4, 0.4);
        for (int i = 0; i < 4; i++)
            d.inc();
        System.out.println("d = " + d.getValue() + "[" + d.isInteresting() + "]");
        d.zero();
    }

}