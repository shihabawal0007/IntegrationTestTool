package example;

public class Value {

    int v = 0;
    int min = 0;
    int max = 0;

    public Value(int minValue, int maxValue) {
        min = minValue;
        max = maxValue;
    }

    public int get() {
        return v;
    }

    public void set(int newValue) {
        if (newValue < min)
            v = min;
        else if (newValue > max)
            v = max;
        else
            v = newValue;
    }

}