package example;
public class OnlineSales {

    public enum Status {
        FULLPRICE,
        DISCOUNT,
        ERROR
    }

    public static Status giveDiscount(long bonusPoints, boolean goldCustomer) {
        Status rv = Status.FULLPRICE;
        long threshold = 120;

        if (bonusPoints <= 0)
            rv = Status.ERROR;

        else {
            if (goldCustomer && bonusPoints != 93) // fault5
                threshold = 80;
            if (bonusPoints > threshold)
                rv = Status.DISCOUNT;
        }

        return rv;
    }

    public static void main(String[] args) {
        Object[][] testData1 = new Object[][] {
                // bonusPoints, goldCustomer
                { 40L, true },
                { 100L, false },
                { 200L, false },
                { -100L, false },
                { 1L, true },
                { 80L, false },
                { 81L, false },
                { 120L, false },
                { 121L, false },
                { Long.MAX_VALUE, false },
                { Long.MIN_VALUE, false },
                { 0L, false },
                { 100L, true },
                { 200L, true },
                { 43L, true },
                { 93L, true },
        };

        for (Object[] test : testData1) {
            System.out.println(giveDiscount((Long) test[0], (Boolean) test[1]));
        }
    }
}
