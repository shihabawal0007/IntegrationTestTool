package tests;

import static org.testng.Assert.*;
import org.testng.annotations.*;

import example.Value;

public class ValueUT {

    @Test
    public void test1() {
        Value v = new Value(-100, 100);
        assertEquals(v.get(), 0);
    }

    @Test
    public void test2() {
        Value v = new Value(-100, 100);
        v.set(22);
        assertEquals(v.get(), 22);
    }

    @Test
    public void test3() {
        Value v = new Value(-100, 100);
        v.set(122);
        assertEquals(v.get(), 100);
    }

    @Test
    public void test4() {
        Value v = new Value(-100, 100);
        v.set(-122);
        assertEquals(v.get(), -100);
    }
}
