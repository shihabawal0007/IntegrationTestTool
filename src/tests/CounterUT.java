package tests;

import static org.testng.Assert.*;
import org.testng.annotations.*;

import example.Counter;

public class CounterUT {

    @Test
    public void test1() {
        Counter c = new Counter();
        assertEquals(c.getValue(), 0);
    }

    @Test
    public void test2() {
        Counter c = new Counter();
        c.add(22);
        assertEquals(c.getValue(), 22);
    }
}
