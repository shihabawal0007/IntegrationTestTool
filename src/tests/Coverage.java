package tests;

import java.lang.Comparable;

import java.lang.annotation.Annotation;
import com.sun.jdi.Method;
import com.sun.jdi.LocalVariable;
import com.sun.jdi.StackFrame;
import com.sun.jdi.Value;
import com.sun.jdi.event.LocatableEvent;
import java.util.List;
import java.util.Map;

public class Coverage /* implements Comparable<Coverage> */ {
    Method caller;
    Method method;
    int level, callLevel;
    Annotation annotation;
    Value returnValue;
    Map<String, Value> params;

    public String toString() {
        String ret = callLevel + " : " + level + " : ";

        if (caller != null) {
            ret += caller + " calls ";
        }

        if (annotation != null) {
            ret += "@" + annotation.annotationType().getSimpleName() + " ";
        }
        ret += method + " with (";
        String args = "";
        if (params != null && params.size() > 0) {
            args += params.toString();
        }
        ret += args + ")";
        if (returnValue != null) {
            ret += " | returns: " + returnValue.toString();
        }
        return ret;
    };

    public String minimalPrint() {
        String ret = "";
        ret += method.name() + "(";
        if (params != null && params.size() > 0) {
            ret += params.toString();
        }
        ret += ") [";
        if (returnValue != null) {
            ret += returnValue.toString();
        }
        ret += "]";
        return ret;
    }

    public boolean isSame(Coverage that) {
        boolean ret = false;
        boolean checkMethod = that.method.toString().equals(this.method.toString());
        boolean checkValues = (that.params != null && this.params != null)
                ? that.params.toString().equals(this.params.toString())
                : true;
        boolean checkReturn = that.returnValue.toString().equals(this.returnValue.toString());
        if (checkMethod && checkValues && checkReturn)
            ret = true;
        return ret;
    }
}
