package tests;

import example.Investor;

public class InvestorTest {

	static void test(boolean expected, long cmp, long pp) {
		boolean actual;
		actual = Investor.sellShares(cmp, pp);
		if (actual != expected)
			System.out.println(
					"Test failed: sellShares(" + cmp + "," + pp + ") returned " + actual + ", expected=" + expected);
	}

	public static void main(String[] args) {

		test(true, 12000, 10000);
		test(true, 10000, 12000);
		test(false, 10000, 10001);
		test(false, 10001, 10000);

	}

}
