package tests;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

import com.sun.jdi.*;
import com.sun.jdi.event.*;
import com.sun.jdi.request.*;
import com.sun.jdi.connect.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicInteger;

public class Integration {

    static boolean logs = false; // flag to print coverage logs
    static boolean verbose = false; // flag to print tree view of calls
    static int MAXDEPTH = 2; // max function call depth for tree view pretty printing

    private static VirtualMachine vm;

    public static void main(String[] args) {
        String classUnderTest = ValueUT.class.getName();
        ArrayList<Coverage> valueCoverage = new ArrayList<>();
        run(classUnderTest, valueCoverage);
        HashSet<Coverage> valueCoverageSet = new HashSet<>();
        valueCoverage.stream().forEach(item -> {
            if (valueCoverageSet.stream().noneMatch(elm -> elm.isSame(item))) {
                valueCoverageSet.add(item);
            } else if (valueCoverage.stream().noneMatch(elm -> elm.isSame(item) && !elm.equals(item))) {
                valueCoverageSet.add(item);
            }
        });
        if (logs) {
            System.out.println("--result--");
            valueCoverage.stream().forEach(System.out::println);
            System.out.println("\n");
            valueCoverageSet.stream().forEach(System.out::println);
            System.out.println(valueCoverageSet.size());
            System.out.println("--result--");
        }

        classUnderTest = CounterUT.class.getName();
        ArrayList<Coverage> counterCoverage = new ArrayList<>();
        run(classUnderTest, counterCoverage);
        HashSet<Coverage> counterCoverageSet = new HashSet<>();
        counterCoverage.stream().forEach(item -> {
            if (counterCoverageSet.stream().noneMatch(elm -> elm.isSame(item))) {
                counterCoverageSet.add(item);
            } else if (counterCoverage.stream().noneMatch(elm -> elm.isSame(item) && !elm.equals(item))) {
                counterCoverageSet.add(item);
            }
        });
        if (logs) {
            System.out.println("--result--");
            counterCoverage.stream().forEach(System.out::println);
            System.out.println("\n");
            counterCoverageSet.stream().forEach(System.out::println);
            System.out.println(counterCoverageSet.size());
            System.out.println("--result--");
        }

        System.out.println("Report\n");
        generateReport(valueCoverageSet, counterCoverageSet);
    }

    public static void generateReport(HashSet<Coverage> base, HashSet<Coverage> toCheck) {
        AtomicInteger total = new AtomicInteger(0);
        AtomicInteger notCovered = new AtomicInteger(0);
        base.stream().filter(item -> item.annotation == null).forEach(item -> {
            Coverage covered = toCheck.stream()
                    .filter(elm -> elm.isSame(item))
                    .findFirst().orElse(null);
            total.getAndIncrement();
            if (covered == null) {
                System.out.println("Not covered: " + item.minimalPrint());
                notCovered.getAndIncrement();
            } else {
                System.out.println("Covered: " + item.minimalPrint());
            }
        });
        System.out.println("Not Covered: " + notCovered.get());
        System.out.println("Total: " + total.get());
        System.out.println("Coverage: " + (double) (total.get() - notCovered.get()) / (double) total.get() * 100.0);
    }

    public static void run(String prog, ArrayList<Coverage> coverage) {
        // Class patterns for which we don't want events
        String[] excludes = { "java.*", "javax.*", "sun.*", "org.testng.*", "jdk.*", "com.*", };

        vm = launchTarget("org.testng.TestNG -testclass " + prog + " -log 2");

        Process process = vm.process();
        StreamRedirectThread errThread = new StreamRedirectThread("error reader", process.getErrorStream(), System.err);
        StreamRedirectThread outThread = new StreamRedirectThread("output reader", process.getInputStream(),
                System.out);
        errThread.start();
        outThread.start();

        EventRequestManager erm = vm.eventRequestManager();

        ClassPrepareRequest req_class = erm.createClassPrepareRequest();
        MethodEntryRequest req_entry = erm.createMethodEntryRequest();
        MethodExitRequest req_exit = erm.createMethodExitRequest();

        for (String ex : excludes) {
            req_class.addClassExclusionFilter(ex);
            req_entry.addClassExclusionFilter(ex);
            req_exit.addClassExclusionFilter(ex);
        }
        // req_class.addClassFilter("com.*");
        req_class.enable();
        req_entry.enable();
        req_exit.enable();

        EventQueue eq = vm.eventQueue();

        boolean running = true; // End execution on VM end or death event
        int rootFrameNumber = 0; // track level of calls (frames)

        try {

            int callLevel = 0;
            while (running) {

                EventSet eventSet = eq.remove();

                for (Event event : eventSet) {
                    if (event instanceof VMStartEvent) {
                        System.out.println("VM started");
                    } else if (event instanceof VMDeathEvent || event instanceof VMDisconnectEvent) {
                        System.out.println("VM ended");
                        running = false;
                    } else if (event instanceof ClassPrepareEvent) {
                        ClassPrepareEvent cpe = (ClassPrepareEvent) event;
                        ReferenceType refType = cpe.referenceType();
                    } else if (event instanceof MethodEntryEvent) {
                        MethodEntryEvent mee = (MethodEntryEvent) event;
                        try {
                            java.lang.reflect.Method reflectMethod = CounterUT.class.getMethod(mee.method().name());
                            int framenumber = mee.thread().frameCount();
                            Annotation anot = reflectMethod.getAnnotations()[0];
                            rootFrameNumber = mee.thread().frameCount();
                            Coverage item = new Coverage();
                            item.callLevel = callLevel;
                            item.level = rootFrameNumber;
                            item.annotation = anot;
                            item.method = mee.method();
                            coverage.add(item);
                            callLevel++;
                            if (verbose) {
                                System.out.println(
                                        "@" + reflectMethod.getAnnotations()[0].annotationType().getSimpleName()
                                                + " : (frame:" + mee.thread().frameCount() + "): "
                                                // + mee.thread().frame(0).thisObject() + ": "
                                                + mee.method() + " -> " + mee.method().hashCode());
                            }
                        } catch (NoSuchMethodException e) { // if <Unit Test class name>.class.getMethod() fails i.e.
                                                            // MethodEntryEvent happened outside Unit Test class (all
                                                            // integration test methods) , but inside class filters
                            for (int i = 1; i <= MAXDEPTH; i++) {
                                if (mee.thread().frameCount() == rootFrameNumber + i) {
                                    int framenumber = mee.thread().frameCount();
                                    Coverage item = new Coverage();
                                    item.caller = mee.thread().frame(1).location().method(); // caller is the method 1
                                                                                             // frame before the current
                                                                                             // method
                                    item.callLevel = callLevel;
                                    item.level = mee.thread().frameCount();
                                    item.method = mee.method();
                                    item.params = new HashMap<>();
                                    coverage.add(item);
                                    callLevel++;
                                    if (verbose) {
                                        // tab lenght increases for every call depth
                                        for (int j = 0; j < i; j++) {
                                            System.out.print("| ");
                                        }
                                        System.out
                                                .print(
                                                        "'-calls: (frame:" + mee.thread().frameCount() + "): "
                                                        // + mee.thread().frame(0).thisObject()
                                                                + ": " + mee.method() + " : " + mee.method().name()
                                                                + "(");
                                    }
                                    List<LocalVariable> parameters = mee.method().arguments();
                                    for (LocalVariable p : parameters) {
                                        item.params.put(" [" + p.typeName() + "]" + p.name(),
                                                getLocalValue((MethodEntryEvent) event, p.name()));

                                        if (verbose) {
                                            System.out.print(" [" + p.typeName() + "]" + p.name() + "=");
                                            System.out.print(getLocalValue((MethodEntryEvent) event, p.name()));
                                        }
                                    }
                                    if (verbose) {
                                        System.out.println(") -> " + mee.method().hashCode());
                                    }
                                }
                            }
                            continue;
                        } catch (Exception e) {
                            System.err.println(e);
                            return;
                        }
                    } else if (event instanceof MethodExitEvent) {
                        MethodExitEvent mee = (MethodExitEvent) event;
                        int currentFrame = mee.thread().frameCount();
                        if (mee.method().toString().equals("tests.CounterUT.<init>()"))
                            continue; // to avoid printing the Unit Test constructor return event
                        Collections.reverse(coverage); // reverse the list ...
                        Coverage item = coverage.stream().filter(elm -> elm.method.equals(mee.method())).findFirst()
                                .orElse(null); // ... find first occurance from reversed list (last occurance from
                                               // original list) ...
                        if (item != null) {
                            item.returnValue = mee.returnValue(); // ... check if occurance exists and store its return
                                                                  // value ...
                        }
                        Collections.reverse(coverage); // ... and reverse back the list
                        if (verbose) {
                            for (int i = 0; i < mee.thread().frameCount() - rootFrameNumber; i++) {
                                System.out.print("| ");
                            }
                            System.out.println("<-" + mee.method() + "returns " + mee.returnValue() + " -> "
                                    + mee.method().hashCode());
                        }
                    }
                }

                vm.resume();

            }
            errThread.join();
            outThread.join();
        } catch (Exception e) {
        }

    }

    static LaunchingConnector findLaunchingConnector() {
        List<Connector> connectors = Bootstrap.virtualMachineManager().allConnectors();
        for (Connector connector : connectors) {
            if (connector.name().equals("com.sun.jdi.CommandLineLaunch")) {
                return (LaunchingConnector) connector;
            }
        }
        throw new Error("No launching connector");
    }

    static Map<String, Connector.Argument> connectorArguments(LaunchingConnector connector, String mainArgs) {
        Map<String, Connector.Argument> arguments = connector.defaultArguments();
        Connector.Argument mainArg = (Connector.Argument) arguments.get("main");
        if (mainArg == null) {
            throw new Error("Bad launching connector");
        }
        mainArg.setValue(mainArgs);
        Connector.Argument optionArg = (Connector.Argument) arguments.get("options");
        if (optionArg == null) {
            throw new Error("Bad launching connector");
        }
        // set cp so the vm can find the main class - assuming that it is on the cp
        String cp = System.getProperty("java.class.path");
        optionArg.setValue("-cp \"" + cp + "\"");

        return arguments;
    }

    static VirtualMachine launchTarget(String mainArgs) {
        LaunchingConnector connector = findLaunchingConnector();
        Map<String, Connector.Argument> arguments = connectorArguments(connector, mainArgs);
        try {
            return connector.launch(arguments);
        } catch (IOException exc) {
            throw new Error("Unable to launch target VM: " + exc);
        } catch (IllegalConnectorArgumentsException exc) {
            throw new Error("Internal error: " + exc);
        } catch (VMStartException exc) {
            throw new Error("Target VM failed to initialize: " +
                    exc.getMessage());
        }
    }

    static Value getLocalValue(LocatableEvent e, String name) {
        StackFrame stackFrame;
        Value v = null;
        try {
            stackFrame = e.thread().frame(0);
            LocalVariable localVar = stackFrame.visibleVariableByName(name);
            v = stackFrame.getValue(localVar);
        } catch (Exception e1) {
            v = null;
        }
        return v;
    }

    static Value getValue(LocatableEvent e, String name) {
        Value v = null;
        try {
            ReferenceType refType = e.location().declaringType();
            Field f1 = refType.fieldByName(name);
            ObjectReference o = e.thread().frame(0).thisObject();
            v = o.getValue(f1);
        } catch (Exception e1) {
            e1.printStackTrace();
            v = null;
        }
        return v;
    }

}