package example;
/**
 * Make investment decisions
 */

import static example.Decider.decision.*;

public class Decider {

	public static enum decision { ERROR, SELL, HOLD };
	
	/**
	 * Decide whether to sell or hold shares given a target profit
	 * @param buyPrice - the price the shares were bought at
	 * @param sellPrice - the price you can currently sell the shares for
	 * @return BUY - buy more shares (profit>=target)<br>
	 * SELL - sell the shares<br>
	 * ERROR - invalid input values
	 */
	public static decision decide(long buyPrice, long sellPrice, long target) {
		if ((buyPrice<1) || (sellPrice<1) || (target<0)) return ERROR;
		else if ((sellPrice-buyPrice)>=target) return SELL;
		else return HOLD;
	}
	
}
