package example;

import static example.Decider.decision.*;

/**
 * Make investment decisions for automated trading
 */
public class Investor {

	/**
	 * @param currentMarketPrice - price shares can be sold at
	 * @param purchasePrice - price shares were bought at
	 * @param highMargin - default target is 15% - except for high margin shares at 10%
	 * @return true to buy more shares
	 */
	public static boolean sellShares(long currentMarketPrice, long purchasePrice) {

		long target=purchasePrice/20; // default is 5%
		
		if (Decider.decide(purchasePrice, currentMarketPrice, target)==SELL)
			return true;
		else
			return false;

	}
	
}
